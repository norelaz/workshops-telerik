package homework;

import java.io.*;
import java.math.BigInteger;

public class Solve {
    static String input = "45+(24*(12+3))";
    static Reader reader = new StringReader(input);

    public static void main(String args[]) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//        BufferedReader userInput = new BufferedReader(reader);

        String input = userInput.readLine();

        System.out.println((evaluate(input)));
    }

    public static BigInteger evaluate(String expr) {

        String expression = addSpaces(expr);

        int indexClose = expression.indexOf(")");
        int indexOpen;
        if (indexClose != -1) {
            String substring = expression.substring(0, indexClose);
            indexOpen = substring.lastIndexOf("(");
            substring = substring.substring(indexOpen + 1).trim();
            if (indexOpen != -1) {
                BigInteger result = evaluate(substring);
                expression = expression.substring(0, indexOpen).trim() +
                        " " + result + " " + expression.substring(indexClose + 1).trim();
                return evaluate(expression.trim());
            }
        }

        String operation;
        if (expression.contains(" / ")) {
            operation = "/";
        } else if (expression.contains(" * ")) {
            operation = "*";
        } else if (expression.contains(" + ")) {
            operation = "+";
        } else if (expression.contains(" - ")) {
            operation = "-";
        } else {
            return new BigInteger(expression);
        }

        int index = expression.indexOf(operation);
        if (index != -1) {
            indexOpen = expression.lastIndexOf(" ", index - 2);
            indexOpen = (indexOpen == -1) ? 0 : indexOpen;
            indexClose = expression.indexOf(" ", index + 2);
            indexClose = (indexClose == -1) ? expression.length() : indexClose;
            BigInteger lhs = new BigInteger(expression.substring(indexOpen, index-1));
            BigInteger rhs = new BigInteger(expression.substring(index + 2, indexClose));
            BigInteger result = null;
            switch (operation) {
                case "/":
                    result = lhs.divide(rhs);
                    break;
                case "*":
                    result = lhs.multiply(rhs);
                    break;
                case "-":
                    result = lhs.subtract(rhs);
                    break;
                case "+":
                    result = lhs.add(rhs);
                    break;
                default:
                    break;
            }
            if (indexClose == expression.length()) {
                expression = expression.substring(0, indexOpen) + " " + result
                        + " " + expression.substring(indexClose);

            } else {
                expression = expression.substring(0, indexOpen) + " " + result
                        + " " + expression.substring(indexClose + 1);
            }
        }
        return evaluate(expression.trim());
    }

    private static String addSpaces(String exp) {

        exp = exp.replaceAll("(?<=[0-9()])[\\/]", " / ");
        exp = exp.replaceAll("(?<=[0-9()])[\\*]", " * ");
        exp = exp.replaceAll("(?<=[0-9()])[+]", " + ");
        exp = exp.replaceAll("(?<=[0-9()])[-]", " - ");

//        Keep replacing double spaces with single spaces until your string is properly formatted
        exp = exp.replaceAll(" {2,}", " ");

        return exp;
    }
}