package homework;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class ScroogeMcDuck {
    private static int row;
    private static int newRow;
    private static int collumn;
    private static int newCollumn;
    private static int coins;
    private static int maxValue;

    static String input = "4 3\n" +
            "3 2 4\n" +
            "2 0 3\n" +
            "1 1 5\n" +
            "2 2 5";
    static Reader reader = new StringReader(input);


    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(reader);
        String input = userInput.readLine();
        String[] sizeOfLabyrinth = input.split(" ");
        row = Integer.parseInt(sizeOfLabyrinth[0]);
        collumn = Integer.parseInt(sizeOfLabyrinth[1]);

        int[][] matrix = new int[row][collumn];

        for (int i = 0; i < row; i++) {
            String line = userInput.readLine();
            String[] numbers = line.split(" ");
            matrix[i] = new int[collumn];
            for (int j = 0; j < collumn; j++) {
                matrix[i][j] = Integer.parseInt(numbers[j]);
            }
        }

        int currentRow = 0;
        int currentCol = 0;

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < collumn; j++) {
                if (matrix[i][j] == 0) {
                    currentRow = i;
                    currentCol = j;
                }
            }
        }
        countCoins(matrix, currentRow, currentCol);
    }

    private static void countCoins(int[][] matrix, int currRow, int currCol) {
        //Move left
        if (currCol - 1 > -1) {
            if (matrix[currRow][currCol - 1] > maxValue) {
                newRow = currRow;
                newCollumn = currCol - 1;
                maxValue = matrix[newRow][newCollumn];
            }
        }

        //Move right
        if (currCol + 1 < collumn) {
            if (matrix[currRow][currCol + 1] > maxValue) {
                newRow = currRow;
                newCollumn = currCol + 1;
                maxValue = matrix[newRow][newCollumn];
            }
        }

        //Move up
        if (currRow - 1 > -1) {
            if (matrix[currRow - 1][currCol] > maxValue) {
                newRow = currRow - 1;
                newCollumn = currCol;
                maxValue = matrix[newRow][newCollumn];
            }
        }

        //Move down
        if (currRow + 1 < row) {
            if (matrix[currRow + 1][currCol] > maxValue) {
                newRow = currRow + 1;
                newCollumn = currCol;
                maxValue = matrix[newRow][newCollumn];
            }
        }

        matrix[newRow][newCollumn] = matrix[newRow][newCollumn] - 1;
        coins++;

        if (matrix[newRow][newCollumn] == 0) {
            System.out.println(coins);
            return;
        }
        countCoins(matrix, newRow, newCollumn);
    }
}

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//
//public class ScroogeMcDuck {
//    static int rows;
//    static int cols;
//    static int coins;
//    static int maxValue;
//
//    public static void main(String[] args) throws IOException {
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//        String[] matrixSize = userInput.readLine().split(" ");
//        rows = Integer.parseInt(matrixSize[0]);
//        cols = Integer.parseInt(matrixSize[1]);
//        int[][] matrix = new int[rows][cols];
//        int currRow = 0;
//        int currCol = 0;
//
//
//        for (int i = 0; i < rows; i++) {
//            String[] row = userInput.readLine().split(" ");
//            for (int j = 0; j < cols; j++) {
//                matrix[i][j] = Integer.parseInt(row[j]);
//                if (matrix[i][j] == 0) {
//                    currRow = i;
//                    currCol = j;
//                }
//            }
//        }
//
//        countCoins(matrix, currRow, currCol);
//    }
//
//    static int newRow = 0;
//    static int newCollumn = 0;
//
//    private static void countCoins(int[][] matrix, int currRow, int currCol) {
//        //Move left
//        if (currCol - 1 > -1) {
//            if (matrix[currRow][currCol - 1] > maxValue) {
//                newRow = currRow;
//                newCollumn = currCol - 1;
//                maxValue = matrix[newRow][newCollumn];
//            }
//        }
//
//        //Move right
//        if (currCol + 1 < cols) {
//            if (matrix[currRow][currCol + 1] > maxValue) {
//                newRow = currRow;
//                newCollumn = currCol + 1;
//                maxValue = matrix[newRow][newCollumn];
//            }
//        }
//
//        //Move up
//        if (currRow - 1 > -1) {
//            if (matrix[currRow - 1][currCol] > maxValue) {
//                newRow = currRow - 1;
//                newCollumn = currCol;
//                maxValue = matrix[newRow][newCollumn];
//            }
//        }
//
//        //Move down
//        if (currRow + 1 < rows) {
//            if (matrix[currRow + 1][currCol] > maxValue) {
//                newRow = currRow + 1;
//                newCollumn = currCol;
//                maxValue = matrix[newRow][newCollumn];
//            }
//        }
//
//        matrix[newRow][newCollumn] = matrix[newRow][newCollumn] - 1;
//        coins++;
//
//        if (matrix[newRow][newCollumn] == 0 && newRow == 0 && newCollumn == 0) {
//            System.out.println(coins);
//            return;
//        }
//
//        countCoins(matrix, newRow, newCollumn);
//    }
//}
