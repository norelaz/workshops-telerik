package judge.solutions.contest1;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class CokiSkoki {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        String[] buildingsStr = scanner.nextLine().split(" ");

        int[] buildings = new int[n];
        for (int i = 0; i < n; i++) {
            buildings[i] = Integer.parseInt(buildingsStr[i]);
        }

        int[] result = new int[n];
        Stack<Integer> stack = new Stack<>();
        for (int i = n - 1; i >= 0; i--) {
            while (!stack.isEmpty() && buildings[i] >= buildings[stack.peek()]) {
                stack.pop();
            }

            if (!stack.isEmpty()) {
                result[i] = result[stack.peek()] + 1;
            }
            stack.push(i);
        }

        StringBuilder output = new StringBuilder();
        output.append(Arrays.stream(result).max().getAsInt());
        output.append("\n");
        for (int x : result) {
            output.append(x);
            output.append(" ");
        }

        System.out.println(output);
    }
}