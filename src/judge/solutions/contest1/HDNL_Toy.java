package judge.solutions.contest1;

import java.util.*;

public class HDNL_Toy {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        Stack<Character> tags = new Stack<>();
        Stack<Integer> levels = new Stack<>();

        StringBuilder output = new StringBuilder();

        int currentLevel = 0;
        for (int i = 0; i < n; i++) {
            String next = scanner.nextLine();
            char tag = next.charAt(0);
            int level = Integer.parseInt(next.substring(1));

            while (!levels.isEmpty() && levels.peek() >= level) {
                currentLevel--;
                printTag(currentLevel, "</%c%d>\n", tags.pop(), levels.pop(), output);
            }

            printTag(currentLevel, "<%c%d>\n", tag, level, output);

            tags.push(tag);
            levels.push(level);
            currentLevel++;
        }

        while (!levels.isEmpty()) {
            currentLevel--;
            printTag(currentLevel, "</%c%d>\n", tags.pop(), levels.pop(), output);
        }

        System.out.println(output);
    }

    private static void printTag(int currentLevel, String format, char tag, int level, StringBuilder output) {
        printIndent(currentLevel, output);
        output.append(String.format(format, tag, level));
    }

    private static void printIndent(int level, StringBuilder output) {
        for (int i = 0; i < level; i++) {
            output.append(" ");
        }
    }
}