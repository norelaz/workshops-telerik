package judge.contest2.greaterMoney;

import java.io.*;
import java.util.*;

public class GreaterMoney {


    public static void main(String[] args) throws IOException {
        String input = "3,5\n" +
                "7,3,1,5";
        Reader reader = new StringReader(input);
        BufferedReader userInput = new BufferedReader(reader);
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

        String[] bag1 = userInput.readLine().split(",");
        String[] bag2 = userInput.readLine().split(",");
        Stack<String> output = new Stack<>();

        for (int i = 0; i < bag1.length; i++) {
            output.push("-1");
            for (int j = 0; j < bag2.length - 1; j++) {
                if (bag1[i].equals(bag2[j])) {
                    for (int k = j + 1; k < bag2.length; k++) {
                        if (Integer.parseInt(bag1[i]) < Integer.parseInt(bag2[k])) {
                            output.pop();
                            output.push(bag2[k]);
                            break;
                        }

                    }
                }
            }
        }

        System.out.println(output.toString()
                .replace("[", "")
                .replace(" ", "")
                .replace("]", ""));
    }
}