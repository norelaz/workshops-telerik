package judge.contest2.studnetsOrder;

import javax.xml.soap.Node;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class StudentsOrder {

    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        String input = userInput.readLine();
        String[] count = input.split(" ");
        int k = Integer.parseInt(count[1]);
        String leftStudent;
        String rightStudent;
        int leftSeatIndex;
        int rightSeatIndex;
        List<String> students = new ArrayList<>(Arrays.asList(userInput.readLine().split(" ")));

        while (k > 0) {
            String[] seatChanges = userInput.readLine().split(" ");
            leftStudent = seatChanges[0];
            rightStudent = seatChanges[1];
            leftSeatIndex = students.indexOf(leftStudent);
            rightSeatIndex = students.indexOf(rightStudent);
            students.add(rightSeatIndex, leftStudent);

            if (rightSeatIndex > leftSeatIndex) {
                students.remove(leftSeatIndex);
            } else {
                students.remove(leftSeatIndex + 1);
            }
            k--;
        }
        for (String student : students) {
            System.out.print(student + " ");
        }
    }
}
