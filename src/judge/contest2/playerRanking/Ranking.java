package judge.contest2.playerRanking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class Ranking {

    static class Player implements Comparable<Player> {

        String name;
        String type;
        int age;

        public Player(String name, String type, int age) {
            this.name = name;
            this.type = type;
            this.age = age;
        }

        String getName() {
            return name;
        }

        String getType() {
            return type;
        }

        int getAge() {
            return age;
        }

        @Override
        public String toString() {
            return String.format("%s(%d)", name, age);
        }

        @Override
        public int compareTo(Player p) {
            int result = getName().compareTo(p.name);
            if (result == 0) {
                result = Integer.compare(p.age, getAge());
            }
            return result;
        }
    }

    private static List<Player> players = new ArrayList<>();
    private static HashMap<String, TreeSet<Player>> playersByType = new HashMap<>();
    private static StringBuilder output = new StringBuilder();

    public static void main(String[] args) throws IOException {

        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        String[] command;

        while (true) {
            String input = userInput.readLine();
            if (input.equals("end")) {
                System.out.println(output);
                break;
            }
            command = input.split(" ");

            switch (command[0]) {
                case "add":
                    addPlayer(command);
                    break;
                case "find":
                    findPlayer(command);
                    break;
                case "ranklist":
                    ranklist(command);
                    break;
            }
        }
    }

    private static void addPlayer(String[] args) {
        String name = args[1];
        String type = args[2];
        int age = Integer.parseInt(args[3]);
        int position = Integer.parseInt(args[4]);

        Player player = new Player(name, type, age); // new player inserted

        players.add(position - 1, player);

        if (!playersByType.containsKey(type)) {
            playersByType.put(type, new TreeSet<>());
        }

        playersByType.get(type).add(player);
        output.append(
                String.format("Added player %s to position %d%n",
                        name,
                        position));
    }

    private static void findPlayer(String[] args) {
        String type = args[1];
        output.append(String.format("Type %s: ", type));

        if (playersByType.containsKey(type)) {
            List<String> output = playersByType.get(type)
                    .stream()
                    .limit(5)
                    .map(Player::toString).collect(Collectors.toList());
            Ranking.output.append(String.join("; ", output));
        }
        output.append("\n");
    }

    private static void ranklist(String[] args) {
        int start = Integer.parseInt(args[1]) - 1;
        int end = Integer.parseInt(args[2]) - 1;

        List<String> ranklist = new ArrayList<>();

        for (int i = start; i <= end; i++) {
            ranklist.add(String.format("%d. %s", i + 1, players.get(i)));
        }
        output.append(String.join("; ", ranklist));
        output.append("\n");
    }
}

