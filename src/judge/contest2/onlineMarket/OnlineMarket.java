package judge.contest2.onlineMarket;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class OnlineMarket {

    private static StringBuilder result = new StringBuilder();
    private static Map<String, Product> products = new TreeMap<>();
    private static TreeSet<Product> pricesOfProduct = new TreeSet<>(new PriceNameComparator());
    private static Map<String, List<Product>> productsOrdererByType = new HashMap<>();
    static String input = "add Milk 1.90 dairy\n" +
            "add Yogurt 1.90 dairy\n" +
            "add Notebook 1111.90 technology\n" +
            "add Orbit 0.90 food\n" +
            "add Rakia 11.90 drinks\n" +
            "add Dress 121.90 clothes\n" +
            "add Jacket 49.90 clothes\n" +
            "add Milk 1.90 dairy\n" +
            "add Socks 2.90 clothes\n" +
            "filter by type dairy\n" +
            "filter by price from 1.00 to 2.00\n" +
            "filter by price from 1.50\n" +
            "filter by price to 2.00\n" +
            "filter by type clothes\n" +
            "end";
    static Reader reader = new StringReader(input);

    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(reader);
        //BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            String[] commandLine = userInput.readLine().split(" ");
            String command = commandLine[0];

            String filterByType = "filter by type";


            switch (command) {
                case "add":
                    addProduct(commandLine);
                    break;
                case "filter":
                    filter(commandLine);
                    break;
                case "end":
                    System.out.println(result);
                    return;
            }
        }
    }

    private static void addProduct(String[] commandLine) {
        String name = commandLine[1];
        double price = Double.parseDouble(commandLine[2]);
        String type = commandLine[3];

        Product newProduct = new Product(name, price, type);

        if (products.containsKey(name)) {
            result.append(String.format("Error: Product %s already exists%n", name));
        } else {
            products.put(name, newProduct);
            result.append(String.format("Ok: Product %s added successfully%n", name));
            if (productsOrdererByType.containsKey(type)) {
                productsOrdererByType.get(type).add(newProduct);
            } else {
                productsOrdererByType.put(type, new ArrayList<>(Arrays.asList(newProduct)));
            }
        }

        pricesOfProduct.add(newProduct);


    }

    private static void filter(String[] commandLine) {
        if (commandLine[2].equals("type")) {
            String type = commandLine[3];
            if (!productsOrdererByType.containsKey(type)) {
                result.append(String.format("Error: Type %s does not exists%n", type));
            } else {
                result.append(String.format("Ok: %s%n", productsOrdererByType.get(type)
                        .stream()
                        .sorted(new PriceNameComparator())
                        .limit(10)
                        .map(Product::toString)
                        .collect(Collectors.joining(", "))));
            }
        }
        if (commandLine[3].equals("from") && commandLine.length <= 5) {
            double price = Double.parseDouble(commandLine[4]);
            result.append(String.format("Ok: %s%n", pricesOfProduct
                    .stream()
                    .filter(p -> p.getPrice() >= price)
                    .limit(10)
                    .map(Product::toString)
                    .collect(Collectors.joining(", "))));
        } else if (commandLine.length > 5) {
            double price = Double.parseDouble(commandLine[4]);
            double maxPrice = Double.parseDouble(commandLine[6]);
            result.append(String.format("Ok: %s%n", pricesOfProduct
                    .stream()
                    .filter(p -> p.getPrice() >= price && p.getPrice() <= maxPrice)
                    .limit(10)
                    .map(Product::toString)
                    .collect(Collectors.joining(", "))));
            //do something
        }
        if (commandLine[3].equals("to")) {
            double price = Double.parseDouble(commandLine[4]);
            result.append(String.format("Ok: %s%n", pricesOfProduct
                    .stream()
                    .filter(p -> p.getPrice() <= price)
                    .limit(10)
                    .map(Product::toString)
                    .collect(Collectors.joining(", "))));
        }


    }

    public static class PriceNameComparator implements Comparator<Product> {

        @Override
        public int compare(Product u1, Product u2) {
            if (u1.getPrice() == u2.getPrice()) {
                return u1.getName().compareTo(u2.getName());
            } else {
                return u2.getPrice() < u1.getPrice() ? 1 : -1;
            }
        }
    }

    public static class Product implements Comparable {

        String name;
        double price;
        String type;

        public Product(String name, double price, String type) {
            this.name = name;
            this.price = price;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }

        public String getType() {
            return type;
        }

        @Override
        public String toString() {
            if (this.price == (long) this.price) {
                return String.format("%s(%d)", this.name, (long) this.price);
            } else {
                return String.format("%s(%s)", this.name, this.price);
            }
        }

        @Override
        public int compareTo(Object p) {
            Product product = (Product) p;
            return this.type.compareTo(product.type);
        }

    }

}
