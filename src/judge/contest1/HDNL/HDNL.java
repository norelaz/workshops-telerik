package judge.contest1.HDNL;

import java.util.*;
import java.util.stream.Collectors;
import static java.util.stream.IntStream.*;

public class HDNL {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        in.nextLine();
        String[] inputStringTags = new String[n];
        int[] integerNumbers = new int[inputStringTags.length];
        Queue<String> outputTags = new ArrayDeque<>();
        Set<String> outputString = new HashSet<>();
        Stack<String> closingTagElements = new Stack<>();
        Stack<String> openingTagElements = new Stack<>();
        List<String> stringList = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            inputStringTags[i] = in.nextLine();
            stringList.add("<" + inputStringTags[i] + ">");
        }

        outputTags.offer(stringList.get(0));
        closingTagElements.push(stringList.get(0).replaceFirst("<", "</"));

        for (int i = 0; i < inputStringTags.length; i++) {
            integerNumbers[i] = Integer.parseInt(inputStringTags[i].substring(1));
        }


        //add x tabs to string
        String s = " ";
        int x = 1;
        String tabRepeated = range(0, x).mapToObj(i -> s).collect(Collectors.joining(""));

        for (int i = 1; i < integerNumbers.length; i++) {
            openingTagElements.push(tabRepeated.concat(stringList.get(i)));


            if (integerNumbers[i] > integerNumbers[i - 1]) {
                x++;
                outputTags.offer(openingTagElements.pop());
            } else {
                x--;
                outputTags.offer(closingTagElements.pop());

                outputTags.offer(openingTagElements.pop());
            }
            closingTagElements.push(tabRepeated
                    .concat(stringList.get(i).replace("<", "</")));
        }

        while(!closingTagElements.isEmpty()) {
            outputTags.offer(tabRepeated.concat(closingTagElements.pop()));
        }

        //Print output
        int sizeOfOuputQueue = outputTags.size() - 1;
        for (int i = 0; i <= sizeOfOuputQueue; i++) {
            System.out.println(((ArrayDeque<String>) outputTags).pop());
        }
    }
}