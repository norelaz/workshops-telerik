package judge.contest1.cokiSkoki;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class CokiSkoki1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Stack<Integer> indexStack = new Stack<>();
        int[] building = new int[n];
        int[] jumps = new int[n];

        for (int i = 0; i < building.length; i++) {
            building[i] = scanner.nextInt();
        }
        for (int i = building.length - 1; i >= 0; i--) {
            while (!indexStack.isEmpty() && building[indexStack.peek()] <= building[i]) {
                jumps[indexStack.pop()] = indexStack.size();
            }
            indexStack.push(i);
        }
        while (!indexStack.isEmpty()) {
            jumps[indexStack.pop()] = indexStack.size();
        }
        System.out.println(Arrays.stream(jumps).max().getAsInt());
        System.out.println(Arrays.toString(jumps)
                .replace("[", "")
                .replace("]", "")
                .replace(",", ""));
    }
}