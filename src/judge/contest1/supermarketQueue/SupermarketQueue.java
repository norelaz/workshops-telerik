package judge.contest1.supermarketQueue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;

public class SupermarketQueue {

    private static final String END_MESSAGE = "End";

    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

        String command;
        String[] commandsLine;
        LinkedList<String> people = new LinkedList<>();
        HashMap<String, Integer> hashmap = new HashMap<>();
        String name;
        int position;

        while (true) {
            String input = userInput.readLine();
            if (input.equals(END_MESSAGE)) {
                break;
            }
            commandsLine = input.split(" ");
            command = commandsLine[0];

            switch (command) {
                case "Append":
                    name = commandsLine[1];
                    if (people.offerLast(name)) {
                        System.out.println("OK");
                    }
                    break;
                case "Insert":
                    position = Integer.parseInt(commandsLine[1]);
                    name = commandsLine[2];
                    if (position > people.size()) {
                        System.out.println("Error");
                        break;
                    } else {
                        people.add(position, name);
                        System.out.println("OK");
                    }
                    break;
                case "Find":
                    int counter = 0;
                    name = commandsLine[1];
                    for (String current : people)
                        if (current.equals(name)) {
                            counter++;
                        }
                    System.out.println(counter);
                    break;
                case "Serve":
                    int last = Integer.parseInt(commandsLine[1]);
                    StringBuilder output = new StringBuilder();
                    if (last > people.size()) {
                        System.out.println("Error");
                        break;
                    } else {
                        for (int i = 0; i < last; i++) {
                            output.append(people.pollFirst() + ((i != last - 1) ? " " : ""));
                        }
                        System.out.println(output);
                        break;
                    }
            }
        }
    }
}
