package judge.contest1.unitsOfWork;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class UnitsOfWork {
    private static final String FAIL_ADDED_MESSAGE = "FAIL: %s already exists!";
    private static final String SUCCESS_ADDED_MESSAGE = "SUCCESS: %s added!";
    private static final String END_MESSAGE = "end";
    private static final String FAIL_NOT_FOUND_MESSAGE = "FAIL: %s could not be found!";
    private static final String SUCCESS_REMOVED_MESSAGE = "SUCCESS: %s removed!";
    private static final String RESULT_MESSAGE = "RESULT: %s";

    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

        String command;
        String[] commandsLine;
        HashMap<String, Unit> units = new HashMap<>();
        HashMap<String, List<Unit>> unitTypes = new HashMap<>();
        TreeSet<Unit> unitList = new TreeSet<>(new AttackNameComparator());

        String name;
        String type;
        int attack;
        int numberOfUnits;

        while (true) {
            String input = userInput.readLine();
            if (input.equals(END_MESSAGE)) {
                break;
            }
            commandsLine = input.split(" ");
            command = commandsLine[0];

            switch (command) {
                case "add":
                    name = commandsLine[1];
                    type = commandsLine[2];
                    attack = Integer.parseInt(commandsLine[3]);
                    if (units.keySet().contains(name)) {
                        System.out.println(String.format(FAIL_ADDED_MESSAGE, name));
                        break;
                    }
                    Unit unit = new Unit(name, type, attack);
                    units.put(name, unit);
                    unitList.add(unit);
                    if (unitTypes.keySet().contains(type)) {
                        unitTypes.get(type).add(unit);
                    } else {
                        unitTypes.put(type, new ArrayList<>(Arrays.asList(unit)));
                    }
                    System.out.println(String.format(SUCCESS_ADDED_MESSAGE, name));
                    break;

                case "remove":
                    name = commandsLine[1];
                    if (!units.keySet().contains(name)) {
                        System.out.println(String.format(FAIL_NOT_FOUND_MESSAGE, name));
                        break;
                    }
                    type = units.get(name).getType();
                    unitTypes.get(type).remove(units.get(name));
                    unitList.remove(units.get(name));
                    units.remove(name);
                    System.out.println(String.format(SUCCESS_REMOVED_MESSAGE, name));
                    break;
                case "find":
                    type = commandsLine[1];
                    System.out.print("RESULT:");
                    if (unitTypes.keySet().contains(type)) {
                        System.out.println(String.format(" %s",
                                unitTypes.get(type)
                                        .stream().sorted(new AttackNameComparator())
                                        .map(Unit::toString)
                                        .limit(10)
                                        .collect(Collectors.joining(", "))));
                    } else {
                        System.out.println("");
                    }
                    break;
                case "power":
                    numberOfUnits = Integer.parseInt(commandsLine[1]);
                    System.out.println(String.format(RESULT_MESSAGE,
                            unitList.stream()
                                    .limit(numberOfUnits)
                                    .map(Unit::toString)
                                    .collect(Collectors.joining(", "))));
                    break;
            }
        }
    }

    public static class Unit {
        private static final int MIN_LENGTH = 1;
        private static final int NAME_MAX_LENGTH = 30;
        private static final int TYPE_MAX_LENGTH = 40;
        private static final int MIN_ATTACK = 100;
        private static final int MAX_ATTACK = 1000;
        private static final String INVALID_ATTACK_MESSAGE = "Attack can be any integer between %d and %d, inclusive";
        private static final String INVALID_LENGTH_MESSAGE = "Unit name can be any unique sequence from 1 to %d characters";

        private String name;
        private String type;
        private int attack;

        public Unit(String name, String type, int attack) {
            setName(name);
            setType(type);
            setAttack(attack);
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public int getAttack() {
            return attack;
        }

        @Override
        public String toString() {
            return String.format("%s[%s](%d)", getName(), getType(), getAttack());
        }

        private void setName(String name) {
            isValid(name, NAME_MAX_LENGTH);
            this.name = name;
        }

        private void setType(String type) {
            isValid(type, TYPE_MAX_LENGTH);
            this.type = type;
        }

        private void setAttack(int attack) {
            if (attack < MIN_ATTACK || attack > MAX_ATTACK) {
                throw new IllegalArgumentException(String.format(INVALID_ATTACK_MESSAGE, MIN_ATTACK, MAX_ATTACK));
            }
            this.attack = attack;
        }

        private void isValid(String input, int maxLength) {
            if (input.length() < MIN_LENGTH || input.length() > maxLength) {
                throw new IllegalArgumentException(String.format(INVALID_LENGTH_MESSAGE, maxLength));
            }
        }
    }

    public static class AttackNameComparator implements Comparator<Unit> {

        @Override
        public int compare(Unit u1, Unit u2) {
            if (u1.getAttack() == u2.getAttack()) {
                return u1.getName().compareTo(u2.getName());
            } else {
                return u2.getAttack() > u1.getAttack() ? 1 : -1;
            }
        }
    }
}