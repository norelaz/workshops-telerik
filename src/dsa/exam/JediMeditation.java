package dsa.exam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Queue;

public class JediMeditation {

    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(userInput.readLine());

        String[] jedi = userInput.readLine().split(" ");

        Queue<String> masters = new ArrayDeque<>();
        Queue<String> knights = new ArrayDeque<>();
        Queue<String> padawans = new ArrayDeque<>();

        Queue<String> output = new ArrayDeque<>();

        for (String j : jedi) {
            if (j.contains("M")) {
                masters.offer(j);
            } else if (j.contains("P")) {
                knights.offer(j);
            } else if (j.contains("K")) {
                padawans.offer(j);
            }
        }
        while (!masters.isEmpty()) {
            output.offer(masters.poll());
        }
        while (!padawans.isEmpty()) {
            output.offer(padawans.poll());
        }
        while (!knights.isEmpty()) {
            output.offer(knights.poll());
        }

        System.out.println(output.toString()
                .replace("[", "")
                .replace("]", "")
                .replace(",", ""));

    }
}
