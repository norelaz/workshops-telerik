package dsa.exam;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class OrderSystem {

    private static Map<String, Order> orders = new HashMap<>();
    private static Map<String, List<Order>> ordersByConsumer = new HashMap<>();

    private static List<Order> orderList = new ArrayList<>();

    static class Order {
        String name;
        double price;
        String consumer;

        public Order(String name, double price, String consumer) {
            this.name = name;
            this.price = price;
            this.consumer = consumer;
        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }

        public String getConsumer() {
            return consumer;
        }

        @Override
        public String toString() {
            return String.format("{%s;%s;%.2f}", name, consumer, price);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Order order = (Order) o;
            return Objects.equals(consumer, order.consumer);
        }

        @Override
        public int hashCode() {
            return Objects.hash(consumer);
        }

        static String input = "10\n" +
                "AddOrder IdeaPad Z560;1536.50;Ivan\n" +
                "AddOrder ThinkPad T410;3000;Ivan\n" +
                "AddOrder VAIO Z13;4099.99;Pesho\n" +
                "AddOrder CLS 63 AMG;200000;Gosho\n" +
                "AddOrder 320i;10000;Tosho\n" +
                "AddOrder G560;999;Ivan\n" +
                "FindOrdersByConsumer Ivan\n" +
                "DeleteOrders Ivan\n" +
                "FindOrdersByConsumer Ivan\n" +
                "FindOrdersByPriceRange 100000;200000";
        static Reader reader = new StringReader(input);

        private static StringBuffer result = new StringBuffer();

        public static void main(String[] args) throws IOException {
//            BufferedReader userInput = new BufferedReader(reader);
            BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//            Scanner scanner = new Scanner(System.in);
            int n = Integer.parseInt(userInput.readLine());

            while (n > 0) {
                String commandLine = userInput.readLine();
                String[] commands = commandLine.split(" ");
                String command = commands[0];

                switch (command) {
                    case "AddOrder":
                        addOrder(commandLine);
                        break;

                    case "DeleteOrders":
                        removeOrders(commands);
                        break;

                    case "FindOrdersByPriceRange":
                        filterFromTo(commandLine);
                        break;

                    case "FindOrdersByConsumer":
                        findOrdersByConsumer(commandLine);
                }
                n--;
            }
            System.out.print(result);
        }

        private static void removeOrders(String[] commands) {
            String consumer;
            if (commands.length > 2) {
                consumer = commands[1] + " " + commands[2];
            } else {
                consumer = commands[1];
            }

            int count = 0;
            if (ordersByConsumer.containsKey(consumer)) {
                for (Order o : orderList) {
                    if (o.getConsumer().equals(consumer)) {
                        count++;
                    }
                }
                ordersByConsumer.remove(consumer);
            }

            result.append((String.format("%d orders deleted", count)));
            result.append("\n");

        }

        private static void filterFromTo(String commandLine) {
            String[] newCommandLine = commandLine.split(";");
            double minValue = Double.parseDouble(newCommandLine[0].substring(23));
            double maxValue = Double.parseDouble(newCommandLine[1]);

            result.append((orderList.stream()
                    .filter(o -> o.getPrice() >= minValue && o.getPrice() <= maxValue)
                    .map(Order::toString).sorted()
                    .collect(Collectors.joining("\n"))));
        }


        private static void addOrder(String commandLine) {

            String[] newCommandLine = commandLine.split(";");

            String name = newCommandLine[0].substring(9);
            double price = Double.parseDouble(newCommandLine[1]);
            String consumer = newCommandLine[2];

            Order newOrder = new Order(name, price, consumer);

            orders.put(name, newOrder);
//            orderList.add(newOrder);

            if (!ordersByConsumer.containsKey(consumer)) {
                ordersByConsumer.put(consumer, new ArrayList<>());
            }

            orderList.add(newOrder);
            ordersByConsumer.get(consumer).add(newOrder);
            result.append("Order added");
            result.append("\n");
//            System.out.println("Order added");

        }

        private static void findOrdersByConsumer(String commandLine) {
            String[] newCommands = commandLine.split(" ");
            String consumer;
            if (newCommands.length > 2) {
                consumer = newCommands[1] + " " + newCommands[2];
            } else {
                consumer = newCommands[1];
            }

            if (ordersByConsumer.containsKey(consumer)) {
                String resultOrders = ordersByConsumer.get(consumer)
                        .stream()
                        .sorted(new NameComparator())
                        .map(Order::toString)
                        .collect(Collectors.joining("\n"));

                result.append(resultOrders);
                result.append("\n");
//                resultOrders.forEach(System.out::println);
            } else {
                result.append("No orders found");
                result.append("\n");
//                System.out.println("No orders found");
            }

        }

        public static class NameComparator implements Comparator<Order> {

            @Override
            public int compare(Order o1, Order o2) {
                return o1.getName().compareTo(o2.getName());
            }
        }
    }
}

