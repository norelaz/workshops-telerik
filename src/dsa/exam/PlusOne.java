package dsa.exam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;

public class PlusOne {
    static ArrayDeque<Long> queue = new ArrayDeque<>();
    static ArrayDeque<Long> result = new ArrayDeque<>();

    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        String[] numbers = userInput.readLine().split(" ");
        long s1 = Long.parseLong(numbers[0]);
        long s2 = Long.parseLong(numbers[1]);

        calculateSequence(s1, s2);
        System.out.print(result.pollLast());
    }

    static void calculateSequence(long n, long seqLength) {
        queue.add(n);

        for (int i = 0; i < seqLength; i++) {
            long current = queue.remove();
            result.add(current);
            long s1 = current + 1;
            long s2 = 2 * current + 1;
            long s3 = current + 2;

            queue.add(s1);
            queue.add(s2);
            queue.add(s3);
        }
    }
}