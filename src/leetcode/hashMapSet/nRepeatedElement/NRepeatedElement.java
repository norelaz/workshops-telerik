package leetcode.hashMapSet.nRepeatedElement;

import java.util.HashSet;
import java.util.Set;

public class NRepeatedElement {
    public static void main(String[] args) {
        int[] nums = {2, 1, 2, 5, 3, 2};
        System.out.println(repeatedNTimes(nums));
    }

    public static int repeatedNTimes(int[] A) {
        Set<Integer> set = new HashSet<>();
        int duplicate = 0;
        for (Integer i : A) {
            if (!set.add(i)) {
                duplicate = i;
            }
        }
        return duplicate;
    }
}
