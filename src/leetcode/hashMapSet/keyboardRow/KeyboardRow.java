package leetcode.hashMapSet.keyboardRow;

import java.util.*;

public class KeyboardRow {
    public static void main(String[] args) {
        String[] input = new String[]{"Hello", "Alaska", "Dad", "Peace"};
        String[] input2 = new String[]{"adsdf", "sfd"};
        System.out.println(Arrays.toString(findWords(input2)));
    }

    private static String rowOne = "qwertyuiopQWERTYUIOP";
    private static String rowTwo = "asdfghjklASDFGHJKL";
    private static String rowThree = "zxcvbnmZXCVBNM";
    private static List<String> output = new ArrayList<>();


    public static String[] findWords(String[] words) {
        output.clear();

        for (String word : words) {
            if (canInput(word))
                output.add(word);
        }

        String[] results = new String[output.size()];
        return output.toArray(results);
    }

    private static boolean canInput(String word) {

        char c = word.charAt(0);
        String checkRow = rowThree;
        if (rowOne.indexOf(c) >= 0) {
            checkRow = rowOne;
        } else if (rowTwo.indexOf(c) >= 0) {
            checkRow = rowTwo;
        }

        for (int i = 1; i < word.length(); i++) {
            char ch = word.charAt(i);
            if (checkRow.indexOf(ch) < 0) {
                return false;
            }
        }
        return true;
    }

}
