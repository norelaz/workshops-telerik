package leetcode.hashMapSet.uncommonWords;

import java.util.*;

public class UncommonWords {
    public static void main(String[] args) {
        String a = "this apple is sweet";
        String a2 = "apple apple";
        String b = "this apple is sour";
        String b2 = "banana";
        System.out.println(Arrays.toString(uncommonFromSentences(a, b)));
    }

    public static String[] uncommonFromSentences(String A, String B) {
        List<String> output = new ArrayList<>();
        Map<String, Integer> hashmap = new HashMap<>();
        for (String word : A.split(" ")) {
            hashmap.put(word, hashmap.getOrDefault(word, 0) + 1);
        }
        for (String word : B.split(" ")) {
            hashmap.put(word, hashmap.getOrDefault(word, 0) + 1);
        }

        for (String word : hashmap.keySet()) {
            if (hashmap.get(word) == 1) {
                output.add(word);
            }
        }
        String[] result = new String[output.size()];
        return output.toArray(result);
    }
}
