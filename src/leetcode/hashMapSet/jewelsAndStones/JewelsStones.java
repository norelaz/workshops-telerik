package leetcode.hashMapSet.jewelsAndStones;

import java.util.*;

class JewelsStones {
    public static void main(String[] args) {

    }

    public static int numJewelsInStones(String J, String S) {
        Set<Character> jewels = new HashSet<>();
        for (Character c : J.toCharArray()) {
            jewels.add(c);
        }
        int count = 0;
        for (Character c : S.toCharArray()) {
            if (jewels.contains(c)) {
                count++;
            }
        }
        return count;
    }

    public static int[] findErrorNums(int[] nums) {
        Set<Integer> set = new HashSet<>();
        int duplicate = 0;
        int missing = 0;
        for (Integer i : nums) {
            if (!set.add(i)) {
                duplicate = i;
                missing = i + 1;
            }
        }
        return new int[]{duplicate, missing};
    }
}
