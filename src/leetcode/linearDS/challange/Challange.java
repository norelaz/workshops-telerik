package leetcode.linearDS.challange;

import java.util.Scanner;
import java.util.Stack;

public class Challange {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] arrInput = input.split("");
        Stack<Integer> indexes = new Stack<>();

        for (int i = 0; i < arrInput.length; i++) {
            if (arrInput[i].equals("(")) {
                indexes.push(i);
            }
            if (arrInput[i].equals(")")) {
                indexes.push(i);
                print(indexes, arrInput);
            }
        }
    }

    private static void print(Stack<Integer> indexes, String[] input) {
        if (indexes.size() < 2) {
            System.out.println("Invalid");
        }
        int endIndex = indexes.pop();
        int startIndex = indexes.pop();
        for (int i = startIndex; i < endIndex + 1; i++) {
            System.out.print(input[i]);
        }
        System.out.println();
    }
}

