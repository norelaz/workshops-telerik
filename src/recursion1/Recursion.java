package recursion1;

//RecursionPath02 - Bunny Ears
//import java.io.*;
//
//public class Recursion {
//    public static void main(String[] args) throws IOException {
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//        int inputNumber = Integer.parseInt(userInput.readLine());
//
//
//        System.out.println(bunnyEars(inputNumber));
//
//    }
//
//    static long bunnyEars(int n) {
//        long result;
//        if (n == 0) {
//            return 0;
//        }
//        if (n == 1) {
//            return 2;
//        } else {
//            result = bunnyEars(n - 1) + 2;
//            return result;
//        }
//    }
//}

//RecursionPath01 - Factorial
//import java.io.*;
//
//public class Recursion {
//
//    public static void main(String[] args) throws IOException {
//        String input = "5\n";
//        Reader reader = new StringReader(input);
////        BufferedReader userInput = new BufferedReader(reader);
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//        int inputNumber = Integer.parseInt(userInput.readLine());
//        long[] array = new long[inputNumber + 1];
//        System.out.println(factorialRec(inputNumber, array));
//    }
//
//    static long factorialRec(int n, long[] mem) {
//        if (n == 1) {
//            return mem[n] = 1;
//        }
//        return mem[n] = factorialRec(n - 1, mem) * n;
//    }
//}

//RecursionPath03 - Fibonacci

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//
//public class Recursion {
//
//    public static void main(String[] args) throws IOException {
//        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
//        int inputNumber = Integer.parseInt(input.readLine());
//        long[] mem = new long[inputNumber + 1];
//        System.out.println(fibonacci(inputNumber, mem));
//    }
//
//    static long fibonacci(int n, long[] mem) {
//        if (n == 0 || n < 0) {
//            return 0;
//        }
//        if (n == 1 || n == 2) {
//            return mem[n] = 1;
//        }
//        if (mem[n] == 0) {
//            mem[n] = fibonacci(n - 1, mem) + fibonacci(n - 2, mem);
//        }
//        return mem[n];
//    }
//}

//RecursionPath04 - Bunny Ears 2
//import java.io.*;
//
//public class Recursion {
//    public static void main(String[] args) throws IOException {
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//        int inputNumber = Integer.parseInt(userInput.readLine());
//
//        System.out.println(bunnyEars2(inputNumber));
//    }
//
//    static int bunnyEars2(int n) {
//        // base case
//        if (n < 1) {
//            return 0;
//        }
//        if (n % 2 == 1) {
//            return 2 + bunnyEars2(n - 1);
//        }
//        return 3 + bunnyEars2(n - 1);
//    }
//}

//RecursionPath05 - Triangle

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//
//public class Recursion {
//    public static void main(String[] args) throws IOException {
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//        int inputNumber = Integer.parseInt(userInput.readLine());
//        long[] arr = new long[inputNumber + 1];
//
//        System.out.println(triangleRec(inputNumber, arr));
//    }
//
//    static long triangleRec(int n, long[] mem) {
//        if (n < 2) {
//            return n;
//        }
//        return n + triangleRec(n - 1, mem);
//    }
//}

//RecursionPath06 - Sum Digits

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//
//public class Recursion {
//    public static void main(String[] args) throws IOException {
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//        int inputNumber = Integer.parseInt(userInput.readLine());
//
//        System.out.println(sumNumbers(inputNumber));
//    }
//
//    static int sumNumbers(int n) {
//        int result;
//        if (n == 0) {
//            return 0;
//        }
//        result = (n % 10 + sumNumbers(n / 10));
//        return result;
//    }
//}

//RecursionPath07 - Count 7

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//
//public class Recursion {
//    public static void main(String[] args) throws IOException {
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//        int inputNumber = Integer.parseInt(userInput.readLine());
//
//
//        System.out.println(countNumbers(inputNumber));
//    }
//
//    static int countNumbers(int x) {
//        int result;
//        if (x == 0) {
//            return 0;
//        }
//        result = (x % 10 + countNumbers(x / 10));
//        return result;
//    }
//}

//RecursionPath08 - Count 8
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//
//public class Recursion {
//    public static void main(String[] args) throws IOException {
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//        int inputNumber = Integer.parseInt(userInput.readLine());
//        int y = 8;
//
//        System.out.println(countOccurance8(inputNumber, y));
//    }
//
//    static int countOccurance8(int n, int y) {
//        if (n == 0) {
//            return 0;
//        }
//        if (n % 10 == y) {
//            if (n / 10 % 10 == y) {
//                return 2 + countOccurance8(n / 10, y);
//            }
//            return 1 + countOccurance8(n / 10, y);
//        }
//        return countOccurance8(n / 10, y);
//    }
//}

//RecursionPath09 - Power N

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//
//public class Recursion {
//    public static void main(String[] args) throws IOException {
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//        int inputBase = Integer.parseInt(userInput.readLine());
//        int inputPowerN = Integer.parseInt(userInput.readLine());
//        long[] array = new long[inputBase + 1];
//
//        System.out.println(powerN(inputBase, inputPowerN, array));
//    }
//
//    static long powerN(int base, int n, long[] mem) {
//        if (n == 0) {
//            return 1;
//        }
//        mem[base] = (base * powerN(base, n - 1, mem));
//        return mem[base];
//    }
//}

//RecursionPath10 - Count X

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//
//public class Recursion {
//    public static void main(String[] args) throws IOException {
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//        String input = userInput.readLine();
//        char a = 'x';
//
//        System.out.println(countX(input, a));
//    }
//
//    static int countX(String str, char a) {
//        if (str.length() == 0) {
//            return 0;
//        }
//        int count = 0;
//        if (str.charAt(0) == a) {
//            count++;
//        }
//        return count + countX(str.substring(1), a);
//    }
//}

//RecursionPath11 - Count Hi

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//
//public class Recursion {
//    public static void main(String[] args) throws IOException {
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//        String input = userInput.readLine();
//        String searchString = "hi";
//
//        System.out.println(countSubstrig(input, searchString));
//    }
//
//    static int countSubstrig(String input, String searchString) {
//
//        //base case
//        if (input.length() < searchString.length())
//            return 0;
//
//        if (input.substring(0, searchString.length()).equals(searchString))
//            return countSubstrig(input.substring(searchString.length() - 1),
//                    searchString) + 1;
//
//        return countSubstrig(input.substring(searchString.length() - 1),
//                searchString);
//    }
//}

//RecursionPath13 - Change Pi

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//
//public class Recursion {
//    public static void main(String[] args) throws IOException {
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//        String input = userInput.readLine();
//        String pi = "pi";
//
//        System.out.println(changePi(input, pi));
//    }
//
//    static String changePi(String input, String pi) {
//        if (input.length() < pi.length()) {
//            return input;
//        }
//        if (input.endsWith(pi)) {
//            return changePi(input.substring(0, input.length() - pi.length()), pi) + "3.14";
//        }
//        return changePi(input.substring(0, input.length() - 1), pi) + input.charAt(input.length() - 1);
//    }
//}

//RecursionPath15 - Array 6

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.Arrays;
//
//public class Recursion {
//    public static void main(String[] args) throws IOException {
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//
//        String[] inputArray = userInput.readLine().split(",");
//
//        int[] array = Arrays.stream(inputArray).mapToInt(Integer::parseInt).toArray();
//
//        int index = Integer.parseInt(userInput.readLine());
//        int x = 6;
//        System.out.println(searchForX(array, x, index));
//    }
//
//    public static boolean searchForX(int[] arr, int numtoSearch, int index) {
//        if (index == arr.length) {
//            return false;
//        }
//        if (arr[index] == numtoSearch)
//            return true;
//        index++;
//        return searchForX(arr, numtoSearch, index);
//
//    }
//}


//RecursionPath16 - Array 11

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.Arrays;
//
//public class Recursion {
//    public static void main(String[] args) throws IOException {
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//
//        String[] inputArray = userInput.readLine().split(",");
//
//        int[] array = Arrays.stream(inputArray).mapToInt(Integer::parseInt).toArray();
//
//        int index = Integer.parseInt(userInput.readLine());
//        int x = 11;
//        System.out.println(countX(array, x, index));
//    }
//
//    static int countX(int[] arr, int numtoSearch, int index) {
//        int count = 0;
//        if (index == arr.length) {
//            return 0;
//        }
//        if (arr[index] == numtoSearch)
//            count++;
//        return count + countX(arr, numtoSearch, index + 1);
//    }
//}

//RecursionPath17 - Array 220

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.Arrays;
//
//public class Recursion {
//    public static void main(String[] args) throws IOException {
//        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
//
//        String[] inputArray = userInput.readLine().split(",");
//
//        int[] inputNumbers = Arrays.stream(inputArray).mapToInt(Integer::parseInt).toArray();
//
//        int index = Integer.parseInt(userInput.readLine());
//        System.out.println(searchForXby10(inputNumbers, index));
//    }
//
//    static boolean searchForXby10(int[] nums, int index) {
//        if (index >= nums.length - 1) {
//            return false;
//        }
//        if (nums[index + 1] == nums[index] * 10)
//            return true;
//        return searchForXby10(nums, index + 1);
//    }
//}
