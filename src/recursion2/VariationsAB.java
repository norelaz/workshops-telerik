package recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class VariationsAB {
    static List<String> output = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(userInput.readLine());
        String[] input = userInput.readLine().split(" ");

        variations(n, input, new StringBuffer());
        Collections.sort(output);
        output.forEach(System.out::println);
    }

    private static void variations(int n, String[] input, StringBuffer buffer) {
        if (n == 0) {
            output.add(buffer.toString());
            return;
        }

        for (String number : input) {
            buffer.append(number);
            variations(n - 1, input, buffer);
            int start = buffer.length() - number.length();
            int end = buffer.length();
            buffer.delete(start, end);
        }
    }
}

