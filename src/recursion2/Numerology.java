package recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Numerology {

    static int[] digitCount = new int[10];

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();
        List<Integer> number = new ArrayList<>();

        for (int i = 0; i < input.length(); i++) {
            number.add(Character.getNumericValue(input.charAt(i)));
        }

        divine(number);

        for (int digit : digitCount) {
            System.out.print(digit + " ");
        }
    }

    private static void divine(List<Integer> digits) {
        if (digits.size() == 1) {
            digitCount[digits.get(0)] = digitCount[digits.get(0)] + 1;
            return;
        }

        for (int i = 0; i < digits.size() - 1; i++) {
            int a = digits.get(i);
            int b = digits.get(i + 1);
            int temp = magic(a, b);
            digits.set(i + 1, temp);
            digits.remove(i);
            divine(digits);

            digits.set(i, a);
            digits.add(i + 1, b);
        }
    }

    private static int magic(int a, int b) {
        return (a + b) * (a ^ b) % 10;
    }
}

