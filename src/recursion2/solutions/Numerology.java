package recursion2.solutions;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class Numerology {
    private static void fakeInput() {
        String test = "18790314";
        System.setIn(new ByteArrayInputStream(test.getBytes()));
    }

    private static int[] result;

    public static int[] numerology() {
        fakeInput();
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();

        result = new int[10];
        for (int i : result) {
            i = 0;
        }

        int[] digits = new int[8];
        int i = 7;
        while (0 < x) {
            digits[i] = x % 10;
            x /= 10;
            i--;
        }

        countDigits(digits);

        return result;
    }

    private static void countDigits(int[] digits) {
        if (digits.length == 1) {
            result[digits[0]]++;
            return;
        }

        for (int i = 0; i < digits.length - 1; i++) {
            int[] newDigits = new int[digits.length - 1];
            for (int j = 0; j < i; j++) {
                newDigits[j] = digits[j];
            }
            newDigits[i] = calcDigit(digits[i], digits[i + 1]);
            for (int j = i + 2; j < digits.length; j++) {
                newDigits[j - 1] = digits[j];
            }
            countDigits(newDigits);
        }
    }

    private static int calcDigit(int x, int y) {
        return ((x + y) * (x ^ y)) % 10;
    }
}