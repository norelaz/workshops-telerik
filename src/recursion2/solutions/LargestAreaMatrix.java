package recursion2.solutions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LargestAreaMatrix {

    static int largestArea;
    static int currentArea;

    static void calcArea(int[][] matrix, int row, int col, int target, boolean[][] isVisited) {

        if ((col < 0) || (row < 0) ||
                (col >= matrix[0].length) || (row >= matrix.length)) {
            return;
        }

        if (isVisited[row][col]) {
            return;
        }

        if (matrix[row][col] != target) {
            return;
        }

        isVisited[row][col] = true;
        currentArea++;
        calcArea(matrix, row + 1, col, target, isVisited);
        calcArea(matrix, row - 1, col, target, isVisited);
        calcArea(matrix, row, col + 1, target, isVisited);
        calcArea(matrix, row, col - 1, target, isVisited);
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String[] incInp = br.readLine().split(" ");
        int rows = Integer.parseInt(incInp[0]);
        int cols = Integer.parseInt(incInp[1]);

        boolean[][] isVisited = new boolean[rows][cols];

        int[][] matrix = new int[rows][cols];
        for (int i = 0; i < rows; i++) {
            String[] inputRow = br.readLine().split(" ");
            for (int j = 0; j < cols; j++) {
                isVisited[i][j] = false;
                matrix[i][j] = Integer.parseInt(inputRow[j]);
            }
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (!isVisited[i][j]) {
                    calcArea(matrix, i, j, matrix[i][j], isVisited);
                    if (currentArea > largestArea) {
                        largestArea = currentArea;
                    }
                    currentArea = 0;
                }
            }
        }
        System.out.println(largestArea);
    }
}