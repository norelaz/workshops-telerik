package recursion2;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class LargestAreaMatrix {
    private static int currentArea;
    private static int largestArea;

    private static void fakeInput() {
        String test = "5 6\n" +
                "1 3 2 2 2 4\n" +
                "3 3 3 2 4 4\n" +
                "4 3 1 2 3 3\n" +
                "4 3 1 3 3 1\n" +
                "4 3 3 3 1 1";
        System.setIn(new ByteArrayInputStream(test.getBytes()));
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String[] input = br.readLine().split(" ");
        int rows = Integer.parseInt(input[0]);
        int cols = Integer.parseInt(input[1]);

        int[][] matrix = new int[rows][cols];

        boolean[][] isVisited = new boolean[rows][cols];

        for (int i = 0; i < rows; i++) {
            String[] currentLine = br.readLine().split(" ");
            for (int j = 0; j < cols; j++) {
                isVisited[i][j] = false;
                matrix[i][j] = Integer.parseInt(currentLine[j]);
            }
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (!isVisited[i][j]) {
                    calcArea(matrix, i, j, matrix[i][j], isVisited);
                    largestArea = Math.max(currentArea, largestArea);
                    currentArea = 0;
                }
            }
        }
        System.out.println(largestArea);
    }

    private static void calcArea(int[][] matrix, int row, int col, int target, boolean[][] isVisited) {
        if (row < 0 || col < 0 ||
                col >= matrix[0].length || row >= matrix.length) {
            return;
        }
        if (isVisited[row][col]) {
            return;
        }
        if (matrix[row][col] != target) {
            return;
        }
        currentArea++;
        isVisited[row][col] = true;

        calcArea(matrix, row + 1, col, target, isVisited);
        calcArea(matrix, row, col + 1, target, isVisited);
        calcArea(matrix, row - 1, col, target, isVisited);
        calcArea(matrix, row, col - 1, target, isVisited);

    }
}
