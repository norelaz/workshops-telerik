package workshops.stackQueue;

public class StackImpl<T> implements Stack<T> {
    private static final int INITIAL_ARRAY = 16;

    private Object[] stack;
    private int index;

    public StackImpl() {
        stack = new Object[INITIAL_ARRAY];
    }

    //Puts a new value(T elem) on the top of the stack.
    @Override
    public void push(T elem) {
        stack[index] = elem;
        index++;
    }

    //Returns and removes the value from the top of the stack
    @Override
    @SuppressWarnings("unchecked")
    public T pop() {
        if (isEmpty()) {
            throw new IllegalArgumentException();
        }
        T result = (T) stack[index - 1];
        stack[index - 1] = 0;
        index--;
        return result;
    }

    //Returns(but does not remove) the value from the top of the stack
    @Override
    @SuppressWarnings("unchecked")
    public T peek() {
        if (isEmpty()) {
            throw new IllegalArgumentException();
        }
        return (T) stack[index - 1];
    }

    //Returns the number of elements in stack
    @Override
    public int size() {
        return index;
    }

    //Tests if stack has or has no elements
    @Override
    public boolean isEmpty() {
        return index == 0;
    }
}
