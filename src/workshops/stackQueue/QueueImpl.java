package workshops.stackQueue;

public class QueueImpl<T> implements Queue<T> {

    private static final int INITIAL_SIZE = 16;
    private Object[] queue;
    private int index;
    private int front = 0;
    private int back = 0;

    public QueueImpl() {
        queue = new Object[INITIAL_SIZE];
    }

    //Puts a new value(T elem) on the back of the queue.
    @Override
    public void offer(T elem) {
        queue[back] = elem;
        back++;
        index++;
    }

    //Returns and removes the head of this queue
    @Override
    @SuppressWarnings("unchecked")
    public T poll() {
        if (isEmpty()) {
            throw new IllegalArgumentException();
        }
        T result = (T) queue[front];
        queue[front] = 0;
        index--;
        front++;
        return result;
    }

    //Tests if queue has or has no elements
    @Override
    public boolean isEmpty() {
        return index == 0;
    }

    //Returns (but does not remove) the head of this queue
    @Override
    @SuppressWarnings("unchecked")
    public T peek() {
        if (isEmpty()) {
            throw new IllegalArgumentException();
        }
        return (T) queue[front];
    }

    //Returns the number of elements in queue
    @Override
    public int size() {
        return index;
    }
}
