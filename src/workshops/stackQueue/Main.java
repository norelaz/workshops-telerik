package workshops.stackQueue;

public class Main {

    public static void main(String[] args) {
        Queue<Integer> queue = new QueueImpl<>();
        queue.offer(4);
        queue.offer(10);
        queue.offer(112);
        System.out.println("QUEUE:");
        System.out.println(queue.peek());
        System.out.println("Size is:" + queue.size());
        System.out.println(queue.poll());
        System.out.println("Size is:" + queue.size());
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println("Size is:" + queue.size());
        System.out.println(queue.poll());
        System.out.println("Size is:" + queue.size());

        Stack<Integer> stack = new StackImpl<>();
        stack.push(4);
        stack.push(10);
        stack.push(112);
        System.out.println();
        System.out.println("STACK:");
        System.out.println(stack.peek());
        System.out.println("Size is:" + stack.size());
        System.out.println(stack.pop());
        System.out.println("Size is:" + stack.size());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.isEmpty());
    }
}
