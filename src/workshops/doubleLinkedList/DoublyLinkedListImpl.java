package workshops.doubleLinkedList;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class DoublyLinkedListImpl<T> implements DoublyLinkedList<T> {

    private DoubleNode<T> head;
    private DoubleNode<T> tail;
    private int count;

    public DoublyLinkedListImpl() {
        this.head = null;
        this.tail = null;
        this.count = 0;
    }

    public DoublyLinkedListImpl(List<T> list) {
        int size = list.size() - 1;
        if (size != 0) {
            DoubleNode<T> node = new DoubleNode<>(list.get(0));
            head = node;
            tail = node;
            count++;
            for (int i = 1; i < list.size(); i++) {
                DoubleNode<T> newNode = new DoubleNode<>(list.get(i));
                newNode.prev = tail;
                tail.next = newNode;
                tail = newNode;
                newNode.next = null;
                size--;
                count++;
            }
        }
    }

    @Override
    public DoubleNode getHead() {
        return head;
    }

    @Override
    public DoubleNode getTail() {
        return tail;
    }

    @Override
    public int getCount() {
        return count;
    }


    @Override
    public void addFirst(T value) {
        DoubleNode newNode = new DoubleNode(value);
        if (head != null) {
            head.prev = newNode;
            newNode.next = head;
            head = newNode;
        } else {
            newNode.next = null;
            newNode.prev = null;
            head = newNode;
            tail = newNode;
        }

        if (tail == null) {
            tail = newNode;
        }
        count++;
    }

    @Override
    public void addLast(T value) {
        DoubleNode newNode = new DoubleNode(value);
        if (tail != null) {
            tail.next = newNode;
        }
        if (head == null) {
            head = newNode;
        }
        tail = newNode;
        count++;
    }

    @Override
    public void insertBefore(DoubleNode node, T value) {
        if (node == null) {
            throw new NullPointerException();
        }
        DoubleNode newNode = new DoubleNode(value);
        count++;
        if (node == head) {
            newNode.prev = null;
            newNode.next = head;
            head.prev = newNode;
            head = newNode;
            return;
        }
        newNode.prev = node.prev;
        newNode.next = node;
        node.prev.next = newNode;
        node.prev = newNode;
    }

    @Override
    public void insertAfter(DoubleNode node, T value) {
        if (node == null) {
            throw new NullPointerException();
        }
        DoubleNode newNode = new DoubleNode(value);
        if (node == tail) {
            tail = newNode;
        }
        newNode.next = node.next;
        node.next = newNode;
        newNode.prev = node;
        if (newNode.next != null) {
            newNode.next.prev = newNode;
        }
        count++;
    }


    @Override
    public T removeFirst() {
        if (head == null) {
            throw new NullPointerException();
        }
        T data = head.getValue();
        if (head.next == null) {
            head = null;
            tail = null;
        } else {
            head = head.next;
            head.prev = null;
        }
        count--;
        return data;
    }

    @Override
    public T removeLast() {
        if (tail == null) {
            throw new NullPointerException();
        }
        T data = tail.getValue();
        if (head.next == null) {
            head = null;
            tail = null;
        } else {
            tail = tail.prev;
            tail.next = null;
        }
        count--;
        return data;
    }

    @Override
    public DoubleNode find(T value) {
        DoubleNode current = head;
        while (current != null) {
            if (current.getValue() == value) {
                return current;
            }
            current = current.next;
        }
        return null;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            DoubleNode<T> current = head;

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public T next() {
                if (hasNext()) throw new NoSuchElementException();
                T data = current.getValue();
                current = current.next;
                return data;
            }
        };
    }
}
