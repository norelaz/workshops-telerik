package workshops.binarySearchTree;

import java.util.*;

public class BinarySearchTreeImpl implements BinarySearchTree {

    private BinaryTreeNode root;

    public BinarySearchTreeImpl() {
        this.root = null;
    }

    @Override
    public BinaryTreeNode getRoot() {
        return root;
    }

    @Override
    public void insert(int value) {
        root = insertRec(root, value);
    }

    @Override
    public BinaryTreeNode search(int value) {
        return searchRec(root, value);
    }

    @Override
    public List<Integer> inOrder() {
        return inOrderRec(root);
    }

    @Override
    public List<Integer> postOrder() {
        return postOrderRec(root);
    }

    @Override
    public List<Integer> preOrder() {
        return preOrderRec(root);
    }

    @Override
    public List<Integer> bfs() {
        return bfsRec(root);
    }

    @Override
    public int height() {
        return findHeightRec(root);
    }

    @Override
    public BinaryTreeNode remove(int value) {
        return null;
    }

    private BinaryTreeNode insertRec(BinaryTreeNode current, int value) {
        if (current == null) {
            return new BinaryTreeNode(value);
        }
        if (value < current.getValue()) {
            current.setLeftChild(insertRec(current.getLeftChild(), value));
        } else if (value > current.getValue()) {
            current.setRightChild(insertRec(current.getRightChild(), value));
        }
        return current;
    }

    private BinaryTreeNode searchRec(BinaryTreeNode node, int value) {
        if (node == null) {
            return null;
        }

        if (value == node.getValue()) {
            return node;
        }

        return value < node.getValue()
                ? searchRec(node.getLeftChild(), value) : searchRec(node.getRightChild(), value);
    }

    private List<Integer> inOrderRec(BinaryTreeNode root) {
        ArrayList<Integer> result = new ArrayList<>();
        Stack<BinaryTreeNode> stack = new Stack<>();
        BinaryTreeNode p = root;
        while (p != null) {
            stack.push(p);
            p = p.getLeftChild();
        }
        while (!stack.isEmpty()) {
            BinaryTreeNode t = stack.pop();
            result.add(t.getValue());
            t = t.getRightChild();
            while (t != null) {
                stack.push(t);
                t = t.getLeftChild();
            }
        }
        return result;
    }

    private List<Integer> postOrderRec(BinaryTreeNode node) {
        List<Integer> list = new ArrayList<>();
        if (node == null) {
            return new ArrayList<>();
        }

        Stack<BinaryTreeNode> stack = new Stack<>();
        stack.push(node);

        while (!stack.isEmpty()) {
            BinaryTreeNode temp = stack.peek();
            if (temp.getLeftChild() == null && temp.getRightChild() == null) {
                BinaryTreeNode pop = stack.pop();
                list.add(pop.getValue());
            } else {
                if (temp.getRightChild() != null) {
                    stack.push(temp.getRightChild());
                    temp.setRightChild(null);
                }

                if (temp.getLeftChild() != null) {
                    stack.push(temp.getLeftChild());
                    temp.setLeftChild(null);
                }
            }
        }

        return list;
    }

    private ArrayList<Integer> preOrderRec(BinaryTreeNode root) {
        ArrayList<Integer> returnList = new ArrayList<>();
        if (root == null)
            return returnList;
        Stack<BinaryTreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.empty()) {
            BinaryTreeNode n = stack.pop();
            returnList.add(n.getValue());
            if (n.getRightChild() != null) {
                stack.push(n.getRightChild());
            }
            if (n.getLeftChild() != null) {
                stack.push(n.getLeftChild());
            }
        }
        return returnList;
    }

    private ArrayList<Integer> bfsRec(BinaryTreeNode root) {
        ArrayList<Integer> returnList = new ArrayList<>();
        if (root == null)
            return returnList;
        Queue<BinaryTreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            BinaryTreeNode n = queue.poll();
            returnList.add(n.getValue());
            if (n.getLeftChild() != null) {
                queue.offer(n.getLeftChild());
            }
            if (n.getRightChild() != null) {
                queue.offer(n.getRightChild());
            }
        }
        return returnList;
    }

    private int findHeightRec(BinaryTreeNode node) {
        int heightLeft = 0;
        int heightRight = 0;
        if (node == null) {
            return -1;
        }
        if (node.getLeftChild() == null && node.getRightChild() == null) {
            return 0;
        }
        if (node.getLeftChild() != null)
            heightLeft = findHeightRec(node.getLeftChild());
        if (node.getRightChild() != null)
            heightRight = findHeightRec(node.getRightChild());
        if (heightLeft > heightRight) {
            return heightLeft + 1;
        } else {
            return heightRight + 1;
        }
    }

}
